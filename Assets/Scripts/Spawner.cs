﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject spawnOrigin;
	public float spawnRate;
	public float speed;
	public GameObject spawnPrefab;

	private float lastSpawn;

	public void Start () {
		spawnRate = 0.5f;
	}

	public void Update () {
		if (Time.time > spawnRate + lastSpawn) {
			Spawn ();
			lastSpawn = Time.time;
		}
	}

	void Spawn () {
		GameObject ball = initGameObject ();
		ChangeColor (ball);
		SetupSpeed (ball);
	}

	GameObject initGameObject() {
		return Instantiate (spawnPrefab, randomPositionVectorInXYPlane (), Quaternion.identity) as GameObject;
	}

	Vector3 randomPositionVectorInXYPlane () {
		Vector3 origin = spawnOrigin.transform.position;
		Vector3 size = spawnOrigin.transform.localScale;

		Vector3 position = new Vector3 (
			Random.Range(origin.x - size.x, origin.x + size.x), 
			Random.Range(origin.y - size.y, origin.y + size.y), 
			0.0f);

		return position;
	}

	void ChangeColor (GameObject ball) {
		Renderer rend = ball.GetComponent<Renderer>();
		rend.material.color = Color.red;
	}

	void SetupSpeed (GameObject ball) {
		ball.GetComponent<Rigidbody> ().velocity = ball.transform.forward * - speed;
	}

}
